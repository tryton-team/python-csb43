.. SPDX-FileCopyrightText: 2024 <wmj.py@gmx.com>
..
.. SPDX-License-Identifier: LGPL-3.0-or-later

.. csb43 Utils

:mod:`csb43.record`
======================

.. automodule:: csb43.aeb43.record
    :members:

.. .. automodule:: csb43.aeb43.record.context
..     :members:

.. automodule:: csb43.aeb43.record.errors
    :members:


:mod:`csb43.fields`
======================

.. automodule:: csb43.aeb43.fields
    :members:

.. automodule:: csb43.aeb43.fields.currency
    :members:

.. automodule:: csb43.aeb43.fields.date
    :members:

.. automodule:: csb43.aeb43.fields.information_mode
    :members:

.. automodule:: csb43.aeb43.fields.integer
    :members:

.. automodule:: csb43.aeb43.fields.money
    :members:

.. automodule:: csb43.aeb43.fields.nested
    :members:

.. automodule:: csb43.aeb43.fields.string
    :members: