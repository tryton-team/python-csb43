.. SPDX-FileCopyrightText: 2024 <wmj.py@gmx.com>
..
.. SPDX-License-Identifier: LGPL-3.0-or-later

.. csb43 Formats

:mod:`csb43.formats`
======================

.. automodule:: csb43.formats
    :members:
    :undoc-members:
    :exclude-members: FORMATS, TABLIB_FORMATS, DICT_FORMATS, convertFromCsb, convertFromCsb2Dict, convertFromCsb2Tabular

.. autoattribute:: csb43.formats.FORMATS

.. autoattribute:: csb43.formats.TABLIB_FORMATS

.. autoattribute:: csb43.formats.DICT_FORMATS

.. autofunction:: csb43.formats.convertFromCsb

.. autofunction:: csb43.formats.convertFromCsb2Dict

.. autofunction:: csb43.formats.convertFromCsb2Tabular